package org.gpf.fishlord;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


/**
 * 鱼池类继承自Jpanel
 * @author gaopengfei
 * @date 2015-4-12 下午7:27:54
 */
class Pool extends JPanel {
	
	BufferedImage background = null;	// 游戏界面的背景图片
	Fish fish = null;    				// 鱼
	Fish[] fishs = new Fish[11];		// 鱼对象数组
	Net net = null;						// 渔网
	
	int score = 0;						// 分数
	Font font = new Font("楷体", Font.BOLD, 20);

	public Pool() throws IOException {
		
		/** getClass().getResourceAsStream()方法读取的是src/images包下的资源文件【图片】*/
		background = ImageIO.read(getClass().getResourceAsStream("/images/bg.jpg"));
		
		/* 产生11条鱼，每1条鱼都是1个线程 */
		for (int i = 0; i < 11; i++) {
			if (i < 9) {
				fish = new Fish("fish0" + (i + 1));
			} else {
				fish = new Fish("fish" + (i + 1));
			}
			
			fishs[i] = fish;
			new Thread(fish).start();
		}
	}

	@Override
	public void paint(Graphics g) {
		
		g.drawImage(background, 0, 0, null); 												// 绘制背景
		for (int i = 0; i < fishs.length; i++) {
			Fish tempfish = fishs[i];
			g.drawImage(tempfish.fishImage, tempfish.x, tempfish.y, null); 					// 绘制鱼
		}
		if (net.show) {
			g.drawImage(net.netImage, net.x - net.width / 2, net.y - net.height / 2, null); // 判断渔网是否显示，绘制渔网
		}
		g.setFont(font);
		g.setColor(Color.YELLOW);
		g.drawString("SCORE:", 10, 20);
		g.setColor(Color.MAGENTA);
		g.drawString("      " + score, 10, 20);
	}

	/**
	 * 处理鼠标事件-实现点击鼠标进行捕鱼
	 * @throws Exception
	 */
	public void action() throws Exception {

		net = new Net();

		/**
		 * 实现鼠标适配器避免出现实现鼠标监听器接口或者使用匿名内部类出现的代码冗余
		 * 我们可以根据需要重写自己需要的方法
		 */
		
		MouseAdapter adapter = new MouseAdapter() {
			
			/* 鼠标进入，渔网显示；鼠标移出，渔网不显示 */
			@Override
			public void mouseEntered(MouseEvent e) {
				net.show = true;
			}

			@Override
			public void mouseExited(MouseEvent e) {
				net.show = false;
			}

			/**
			 * 渔网的位置随着鼠标的位置变化
			 */
			@Override
			public void mouseMoved(MouseEvent e) {
				
				net.x = e.getX();
				net.y = e.getY();
			}

			/**
			 * 当鼠标按下的时候进行捕鱼操作
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				catchFish();
			}

		};
		
		this.addMouseListener(adapter); 		// 添加鼠标监听器
		this.addMouseMotionListener(adapter);	// 鼠标移动监听器

		while (true) {
			repaint(); 
			try {
				Thread.sleep(100); 				// 每隔一定时间刷新屏幕，需要符合视觉暂留设置50~100ms
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 渔网是否捉到鱼
	 */
	protected void catchFish() {
		// 鱼在不在网的范围内？在的话就让鱼消失
		for (int i = 0; i < fishs.length; i++) {
			fish = fishs[i];
			if (fish.fishInNet(net.x, net.y)) {// 判断在不在网的范围
				fish.goInPool();				 // 鱼从池子中消失，重新从右边游入	
				score += fish.width / 10;		 // 不同的鱼有不同的分数
			}
		}
	}

}